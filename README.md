# bootstrap-params

This is a repository of parameter choices for bootstrap calculations using sdpb, blocks-3d, scalar-blocks

While the parameters are supposed to be reasonable, your mileage may vary.